# An NginX container that will be the frontend of an ownCloud container with php-fpm
# The container will receive some configuration from environment variables.
#
# Possibilities / example:
#    - NGINX_HTML_ROOT=/var/www/html
#    - NGINX_PORT=80
#    - NGINX_HOST=_
#    - NGINX_FPM_HOST=owncloud
#    - NGINX_FPM_PORT=9000
#
# A file with default values is provided
#
# You may also tweak some values directly in nginx-owncloud.conf file
# 
#

FROM nginx
MAINTAINER Sebastien Collin <sebastien.collin@sebcworks.fr>

COPY nginx-owncloud.conf /tmp/nginx-owncloud.conf
COPY docker-entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["start"]
