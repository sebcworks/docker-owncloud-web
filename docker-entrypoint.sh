#!/bin/bash

set -e

if [ "$1" = 'start' ]; then
    if [ -r '/tmp/nginx-owncloud.conf' ]; then
	DOLLAR='$' envsubst < /tmp/nginx-owncloud.conf > /etc/nginx/conf.d/default.conf
	echo "Configuration file:"
	cat /etc/nginx/conf.d/default.conf
    else
	echo "Can't read site template file, start nginx anyway"
    fi
    # Make nginx run as www-data
    sed 's/^\([[:space:]]*user[[:space:]]*\).*\;[[:space:]]*$/\1www-data\;/' -i /etc/nginx/nginx.conf
    exec nginx -g 'daemon off;'
fi

exec "$@"
