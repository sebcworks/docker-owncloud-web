NginX frontend for ownCloud Container
=====================================

Description
-----------

A custom NginX container used as a frontend for an ownCloud container with php-fpm.

It is based on the [official nginx container](https://hub.docker.com/_/nginx/).

Its purpose is to be used with the owncloud container sebcworks/owncloud (https://hub.docker.com/r/sebcworks/owncloud/) :


Usage
-----

*Easy use with the docker-compose.yml file of the sebcworks/owncloud repos*

    git clone https://sebcworks@bitbucket.org/sebcworks/docker-owncloud.git

    cd docker-owncloud

    cp example-docker-compose.yml docker-compose.yml

    docker-compose up -d
    
Otherwise, use it by changing the values present in the environment file envfile.default or by passing them in the command line.
You will need a running ownCloud container with php-fpm.

    nano envfile.default

    docker run --volumes-from owncloud-container-name --env-file envfile.default sebcworks/owncloud-web
    
